// Import React
import React from "react";
// Import Bootstrap
import "bootstrap/dist/css/bootstrap.css";
// Import Component MainContent
import MainContent from "./component/MainContent/MainContent";

function App() {
  return (
    <React.Fragment>
      <MainContent />
    </React.Fragment>
  );
}

export default App;
