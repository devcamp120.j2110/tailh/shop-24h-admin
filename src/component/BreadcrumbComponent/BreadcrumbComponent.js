// Import MUI
import { Breadcrumbs, Link, Typography } from "@mui/material";

function BreadcrumbComponent(props) {
  return (
    <div role="presentation" className="mb-3">
      <Breadcrumbs aria-label="breadcrumb">
        <Link
          sx={{ cursor: "pointer" }}
          underline="hover"
          color={props.content ? "inherit" : "primary"}
          href="/"
          fontWeight={props.content ? "inherit" : "bold"}
        >
          Dashboard
        </Link>
        {props.content ? (
          <Typography
            sx={{ display: "flex", alignItems: "center", fontWeight: "bold" }}
            color="text.primary"
          >
            {props.content}
          </Typography>
        ) : null}
      </Breadcrumbs>
    </div>
  );
}

export default BreadcrumbComponent;
