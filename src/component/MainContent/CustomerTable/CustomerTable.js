// Hook
import React, { useEffect, useState } from "react";
// CSS
import "./CustomerTable.css";
import {
  Container,
  Grid,
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Button,
  Stack,
  Pagination,
  TextField,
  Typography,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "@mui/material";
// Icon
import { Add, Edit, Search } from "@mui/icons-material";
// Component
import BreadcrumbComponent from "../../BreadcrumbComponent/BreadcrumbComponent";
import CustomerFormModal from "../../Modal/CustomerFormModal/CustomerFormModal";
import ModalError from "../../Modal/ModalError";
import ModalConfirm from "../../Modal/CustomerFormModal/ModalConfirm";
import SnackBar from "../../SnackBar/SnackBar";

function CustomerTable() {
  // Fetch Api
  const fetchApi = async (paramUrl, paramOption = {}) => {
    const response = await fetch(paramUrl, paramOption);
    const responseData = await response.json();
    return responseData;
  };
  // State
  const [addMode, setAddMode] = useState(false);
  const [emailExists, setEmailExists] = useState(false);
  const [customers, setCustomers] = useState("");
  const [customer, setCustomer] = useState("");
  const [filterInput, setFilterInput] = useState({
    fullname: "",
    email: "",
    phoneNumber: "",
  });
  // Lưu giá trị của email trong trường hợp edit
  const [currentEmail, setCurrentEmail] = useState("");
  // Modal
  const [modalCustomer, setModalCustomer] = useState({
    display: false,
  });
  const [modalError, setModalError] = useState({
    display: false,
    content: "",
  });
  const [modalConfirm, setModalConfirm] = useState({
    display: false,
    content: "",
  });
  //SnackBar
  const [snackBar, setSnackBar] = useState({
    display: false,
    content: "",
    severity: "",
  });
  // Pagination
  const [limit, setLimit] = useState(4);
  const [numberOfPage, setNumberOfPage] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [countCustomer, setCountCustomer] = useState(""); //State Đếm tổng số product

  // Event Handle
  // Call Api Check Email Exists
  const callApiCheckEmailCustomer = () => {
    fetchApi("http://localhost:8080/customers/email/" + customer.email)
      .then((data) => {
        console.log(data);
        // Thay đổi state khi lấy đc dữ liệu
        if (data.customer !== null) {
          setEmailExists(true);
        } else {
          setEmailExists(false);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  //Call Api get Customer Count
  const getCustomerCount = () => {
    fetchApi("http://localhost:8080/customers/count")
      .then((data) => {
        console.log(data.number);
        setCountCustomer(data.number); //Call API thành công lưu lại tổng số customer
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Call API get all customer
  const getAllCustomer = (paramLimit, paramSkip) => {
    fetchApi(
      "http://localhost:8080/customers" +
        "?limit=" +
        paramLimit +
        "&skip=" +
        paramSkip
    )
      .then((data) => {
        console.log(data);
        // Set Customer
        setCustomers(data.customers);
        // Tính số trang
        setNumberOfPage(Math.ceil(countCustomer / limit));
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Call API update customer
  const callApiUpdateCustomer = (paramCustomer) => {
    const customer = {
      method: "PUT",
      body: JSON.stringify(paramCustomer),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    };
    fetchApi("http://localhost:8080/customers/" + paramCustomer._id, customer)
      .then((data) => {
        console.log(data);
        // reload table
        getAllCustomer(limit, (currentPage - 1) * limit);
        // Close Modal
        setModalCustomer({
          display: false,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Call API create customer
  const callApiCreateCustomer = (paramCustomer) => {
    const customer = {
      method: "POST",
      body: JSON.stringify(paramCustomer),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    };
    fetchApi("http://localhost:8080/customers", customer)
      .then((data) => {
        console.log(data);
        // reload table
        getAllCustomer(limit, (currentPage - 1) * limit);
        // Close Modal
        setModalCustomer(false);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Hàm xử lý khi chuyển trang
  const changePageHandle = (event, value) => {
    console.log("Change Page");
    setCurrentPage(value);
    window.scrollTo({ top: 0, behavior: "smooth" });
  };
  // Hàm Lưu Giá trị khi ô input thay đổi
  const onInputFullname = (event) => {
    setFilterInput({ ...filterInput, fullname: event.target.value });
  };
  const onInputEmail = (event) => {
    setFilterInput({ ...filterInput, email: event.target.value });
  };
  const onInputPhoneNumber = (event) => {
    setFilterInput({ ...filterInput, phoneNumber: event.target.value });
  };
  // Hàm xử lý khi đổi Limit
  const onChangeLimit = (event) => {
    setLimit(event.target.value);
  };
  // Hàm Search Customer
  const searchCustomer = () => {
    // Call API Search
    fetchApi(
      "http://localhost:8080/customers/customer-filter" +
        "?limit=" +
        "" +
        "&skip=" +
        "" +
        "&fullname=" +
        filterInput.fullname.trim() +
        "&email=" +
        filterInput.email.trim() +
        "&phoneNumber=" +
        filterInput.phoneNumber.trim()
    )
      .then((data) => {
        console.log(data);
        if (
          filterInput.fullname === "" &&
          filterInput.email === "" &&
          filterInput.phoneNumber === ""
        ) {
          getAllCustomer(limit, (currentPage - 1) * limit); //Giữ Nguyên Bảng Cũ khi inpout rỗng
        } else {
          // Nếu có dữ liệu lưu lại state customer
          setCustomers(data.customers);
          setNumberOfPage(1);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Hàm Xử lý khi nhấn váo nút edit
  const onEditButtonClick = (customer) => {
    // Lấy giá trị Email hiện tại
    setCurrentEmail(customer.email);
    // Lưu add Mode
    setAddMode(false);
    // Hiện Modal
    setModalCustomer({
      display: true,
    });
    // Lưu customer vào state
    setCustomer(customer);
  };
  // Hàm xử lý sự kiện khi nhấn nút Update
  const onUpdateCustomerClick = () => {
    // validate data
    let isValidatedCustomerInfo = validateData();
    if (isValidatedCustomerInfo) {
      // Hiển thị modal confirm
      setModalConfirm({
        display: true,
        content: "Are you sure you want to update customer",
      });
    }
  };
  // Hàm xử lý sự kiện khi nhấn nút Add customer
  const onAddButtonClick = () => {
    // Set Add  Mod
    setAddMode(true);
    // Reset State    
    setCustomer("");
    // Hiện Modal
    setModalCustomer({
      display: true,
    });
  };
  // Hàm xử lý sự kiện khi nhấn nút Create
  const onCreateCustomerClick = () => {
    // validate data
    let isValidatedCustomerInfo = validateData();
    if (isValidatedCustomerInfo) {
      // Hiển thị modal confirm
      setModalConfirm({
        display: true,
        content: "Are you sure you want to create customer",
      });
    }
  };
  console.log(emailExists);
  
  // Hàm validate dữ liệu
  const validateData = () => {
    var vEmailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    // Email trường hợp Add Customer
    if (addMode) {
      if (
        customer.email === undefined ||
        customer.email.trim() === "" ||
        !vEmailRegex.test(customer.email) ||
        emailExists === true
      ) {
        setModalError({
          display: true,
          content: "Invalid Email",
        });
        return false;
      }
    }
    // Email trường hợp Edit Customer
    if (!addMode) {
      if (
        customer.email === undefined ||
        customer.email.trim() === "" ||
        !vEmailRegex.test(customer.email) ||
        (emailExists === true && customer.email.trim() !== currentEmail.trim())
      ) {
        setModalError({
          display: true,
          content: "Invalid Email",
        });
        return false;
      }
    }
    // Validate Fulname
    if (customer.fullname === undefined || customer.fullname.trim() === "") {
      setModalError({
        display: true,
        content: "Invalid Fullname",
      });
      return false;
    }
    // Validate Address
    if (customer.address === undefined || customer.address.trim() === "") {
      setModalError({
        display: true,
        content: "Invalid Address",
      });
      return false;
    }
    // Validate Phone Number
    if (
      customer.phoneNumber === undefined ||
      customer.phoneNumber.trim() === ""
    ) {
      setModalError({
        display: true,
        content: "Invalid Phone Number",
      });
      return false;
    }
    if (
      isNaN(customer.phoneNumber === undefined || customer.phoneNumber.trim())
    ) {
      setModalError({
        display: true,
        content: "Invalid Phone Number",
      });
      return false;
    }
    // Validate City
    if (customer.city === undefined || customer.city.trim() === "") {
      setModalError({
        display: true,
        content: "Invalid City",
      });
      return false;
    }
    // Validate Country
    if (customer.country === undefined || customer.country.trim() === "") {
      setModalError({
        display: true,
        content: "Invalid Country",
      });
      return false;
    }

    return true;
  };

  // useEffect thực hiện lại các hàm bên dưới khi các biến trong dependencies thay đổi
  useEffect(() => {
    getCustomerCount();
    callApiCheckEmailCustomer();
    getAllCustomer(limit, (currentPage - 1) * limit);
  }, [countCustomer, limit, currentPage, customer]);
  return (
    <>
      <Container maxWidth="large">
        {/* Breadcrumb , Truyền content vào Breadcrumb*/}
        <BreadcrumbComponent content="Customer"></BreadcrumbComponent>
        {/* Title */}
        <Typography
          fontWeight="bold"
          my={2}
          align="center"
          variant="h3"
          component="h2"
        >
          Customer Manager
        </Typography>
        {/* Search Form */}
        <Grid className="search-form-customer" mb={4} container>
          {/* Name */}
          <Grid p={1} item xs={6}>
            <TextField
              id="outlined-name"
              className="w-100 background-input"
              label="Fullname"
              value={filterInput.fullname}
              onChange={onInputFullname}
            />
          </Grid>
          {/* Phone Number */}
          <Grid p={1} item xs={6}>
            <TextField
              id="outlined-name"
              className="w-100 background-input"
              label="Phone Number"
              value={filterInput.phoneNumber}
              onChange={onInputPhoneNumber}
            />
          </Grid>
          {/* Email */}
          <Grid p={1} item xs={12}>
            <TextField
              id="outlined-name"
              className="w-100 background-input"
              label="Email"
              value={filterInput.email}
              onChange={onInputEmail}
            />
          </Grid>
          {/* Search Button */}
          <Grid p={1} item xs={12}>
            <Button
              sx={{ float: "right", fontWeight: "bold" }}
              size="large"
              variant="contained"
              onClick={() => {
                searchCustomer();
              }}
              startIcon={<Search />}
            >
              Search
            </Button>
          </Grid>
        </Grid>
        {/* Pagination */}
        <Grid container alignItems="end" my={2}>
          <Grid item xs={6}>
            <Stack spacing={2}>
              <Pagination
                onChange={changePageHandle}
                count={numberOfPage}
                defaultPage={currentPage}
                color="primary"
              />
            </Stack>
          </Grid>
          <Grid align="right" item xs={6}>
            <Grid container>
              <Grid item className="d-flex justify-content-end" xs={12}>
                {/* Select Limit */}
                <FormControl>
                  <InputLabel id="select-limit">Limit</InputLabel>
                  <Select
                    labelId="select-limit"
                    value={limit}
                    label="Limit"
                    onChange={onChangeLimit}
                    className="background-input"
                  >
                    <MenuItem value="4">4</MenuItem>
                    <MenuItem value="6">6</MenuItem>
                    <MenuItem value="10">10</MenuItem>
                  </Select>
                </FormControl>
                {/* Add Product Button */}
                <Button
                  sx={{ ml: 1, fontWeight: "bold", float: "right" }}
                  variant="outlined"
                  size="large"
                  startIcon={<Add />}
                  onClick={onAddButtonClick}
                >
                  Add Customer
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        {/* Table */}
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} size="large" aria-label="a dense table">
            <TableHead>
              <TableRow>
                <TableCell>#</TableCell>
                <TableCell className="fw-bold" align="left">
                  Fullname
                </TableCell>
                <TableCell className="fw-bold" align="left">
                  Phone Number
                </TableCell>
                <TableCell className="fw-bold" align="left">
                  Email
                </TableCell>
                <TableCell className="fw-bold" align="left">
                  Address
                </TableCell>
                <TableCell className="fw-bold" align="left">
                  City
                </TableCell>
                <TableCell className="fw-bold" align="left">
                  Country
                </TableCell>
                <TableCell className="fw-bold" align="center">
                  Action
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {customers !== ""
                ? customers.map((customer, index) => (
                    <TableRow
                      key={index}
                      sx={{
                        "&:last-child td, &:last-child th": { border: 0 },
                        maxHeight: "100px",
                        overflow: "hidden",
                      }}
                    >
                      <TableCell component="th" scope="row">
                        {index + 1}
                      </TableCell>
                      <TableCell align="left">{customer.fullname}</TableCell>
                      <TableCell align="left">{customer.phoneNumber}</TableCell>
                      <TableCell align="left">{customer.email}</TableCell>
                      <TableCell align="left">{customer.address}</TableCell>
                      <TableCell align="left">{customer.city}</TableCell>
                      <TableCell align="left">{customer.country}</TableCell>
                      <TableCell align="center">
                        <Button
                          sx={{ fontWeight: "bold" }}
                          variant="contained"
                          size="small"
                          startIcon={<Edit />}
                          onClick={() => {
                            onEditButtonClick(customer);
                          }}
                        >
                          EDIT
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))
                : null}
            </TableBody>
          </Table>
        </TableContainer>
      </Container>
      {/* Modal Form */}
      <CustomerFormModal
        addMode={addMode}
        onUpdateCustomerClick={onUpdateCustomerClick}
        onCreateCustomerClick={onCreateCustomerClick}
        customer={customer}
        setCustomer={setCustomer}
        modalCustomer={modalCustomer}
        setModalCustomer={setModalCustomer}
      ></CustomerFormModal>
      {/* Modal Error */}
      <ModalError
        modalError={modalError}
        setModalError={setModalError}
      ></ModalError>
      {/* Modal Confirm */}
      <ModalConfirm
        addMode={addMode}
        modalConfirm={modalConfirm}
        setModalConfirm={setModalConfirm}
        callApiUpdateCustomer={callApiUpdateCustomer}
        callApiCreateCustomer={callApiCreateCustomer}
        customer={customer}
        setSnackBar={setSnackBar}
      ></ModalConfirm>
      {/* SnackBar */}
      <SnackBar snackBar={snackBar} setSnackBar={setSnackBar}></SnackBar>
    </>
  );
}

export default CustomerTable;
