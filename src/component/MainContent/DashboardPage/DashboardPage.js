// Hook
import React, { useEffect, useState } from "react";
// Recharts
import {
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  AreaChart,
  Area,
} from "recharts";
// Material UI
import { Container, Grid, Typography } from "@mui/material";
// Component
import BreadcrumbComponent from "../../BreadcrumbComponent/BreadcrumbComponent";
function DashboardPage() {
  // State
  const [chartData, setChartData] = useState([]);
  // Fetch Api
  const fetchApi = async (paramUrl, paramOption = {}) => {
    const response = await fetch(paramUrl, paramOption);
    const responseData = await response.json();
    return responseData;
  };
  // Call API get all customer
  const getAllCustomer = () => {
    fetchApi("http://localhost:8080/customers" + "?limit=" + "" + "&skip=" + "")
      .then((res) => {
        console.log(res);
        return res.customers;
      })
      .then((customers) => {
        getAllOrderByCustomer(customers);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Call API get Order By customer
  const getAllOrderByCustomer = (customers) => {
    customers.map((customer, index) => {
      // Call API Search
      fetchApi(
        "http://localhost:8080/orders/filter" +
          "?limit=" +
          "" +
          "&skip=" +
          "" +
          "&customer=" +
          customer._id
      )
        .then((data) => {
          console.log(data);
          setChartData((chartData) => [
            ...chartData,
            {
              name: data.orders[0].customer.fullname,
              order: data.orders.length,
            },
          ]);
        })
        .catch((err) => {
          console.log(err);
        });
    });
  };
  // Use Effect
  useEffect(() => {
    getAllCustomer();
  }, []);
  return (
    <>
      <Container maxWidth="large">
        {/* Breadcrumb */}
        <BreadcrumbComponent></BreadcrumbComponent>
        <Grid justifyContent="center" container>
          <Grid item xs={12}>
            <Typography
              align="center"
              fontWeight="bold"
              variant="h3"
              gutterBottom
              component="div"
            >
              Order Chart
            </Typography>
          </Grid>
          <Grid item xs={12}>
            {/*Area Chart Order*/}
            <AreaChart
              width={1200}
              height={500}
              data={chartData}
              margin={{
                top: 10,
                bottom: 0,
              }}
              className="mx-auto"
            >
              <CartesianGrid strokeDasharray="1 1" />
              <XAxis dataKey="name" />
              <YAxis />
              <Tooltip />
              <Area
                type="monotone"
                dataKey="order"
                stroke="#8884d8"
                fill="#f0f8ff"
              />
            </AreaChart>
          </Grid>
        </Grid>
      </Container>
    </>
  );
}

export default DashboardPage;
