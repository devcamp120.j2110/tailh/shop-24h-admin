import {
  Box,
  Drawer,
  AppBar,
  CssBaseline,
  Toolbar,
  List,
  Typography,
  Divider,
  ListItemIcon,
  ListItemText,
  ListItemButton,
} from "@mui/material";
import { Category, Person, ShoppingCart, Dashboard } from "@mui/icons-material";
import { Routes, Route, useNavigate } from "react-router-dom";
import ProductTable from "./ProductTable/ProductTable";
import DashboardPage from "./DashboardPage/DashboardPage";
import CustomerTable from "./CustomerTable/CustomerTable";
import OrderTable from "./OrderTable/OrderTable";

// CSS
import "./MainContent.css";

function MainContent() {
  const drawerWidth = 240;
  const navigate = useNavigate();
  const navigateDashboard = () => {
    navigate("/");
  };
  const navigateCustomerTable = () => {
    navigate("/customer");
  };
  const navigateProductTable = () => {
    navigate("/product");
  };
  const navigateOrderTable = () => {
    navigate("/order");
  };

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      {/* Nav Bar */}
      <AppBar
        position="fixed"
        sx={{ zIndex: (theme) => theme.zIndex.drawer + 1 }}
      >
        <Toolbar>
          <Typography
            onClick={navigateDashboard}
            sx={{ cursor: "pointer" }}
            variant="h6"
            noWrap
            component="div"
          >
            Kara Shop Admin
          </Typography>
        </Toolbar>
      </AppBar>
      {/* Side Bar */}
      <Drawer
        variant="permanent"
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          [`& .MuiDrawer-paper`]: {
            width: drawerWidth,
            boxSizing: "border-box",
          },
        }}
      >
        <Toolbar />
        <Box sx={{ overflow: "auto" }}>
          <List>
            {/* Dashboard */}
            <ListItemButton
              className="button-sidebar"
              onClick={navigateDashboard}
            >
              <ListItemIcon>
                <Dashboard />
              </ListItemIcon>
              <ListItemText primary="Dashboard" />
            </ListItemButton>
            <Divider />
            {/* Customer */}
            <ListItemButton
              className="button-sidebar"
              onClick={navigateCustomerTable}
            >
              <ListItemIcon>
                <Person />
              </ListItemIcon>
              <ListItemText primary="Customer" />
            </ListItemButton>
            {/* Product */}
            <ListItemButton
              className="button-sidebar"
              onClick={navigateProductTable}
            >
              <ListItemIcon>
                <Category />
              </ListItemIcon>
              <ListItemText primary="Product" />
            </ListItemButton>
            {/* Order */}
            <ListItemButton
              className="button-sidebar"
              onClick={navigateOrderTable}
            >
              <ListItemIcon>
                <ShoppingCart />
              </ListItemIcon>
              <ListItemText primary="Order" />
            </ListItemButton>
          </List>
          <Divider />
        </Box>
      </Drawer>
      {/* Main Content */}
      <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
        <Toolbar />
        {/* Router */}
        <Routes>
          {/* Dashboard */}
          <Route
            exact
            path="/"
            element={<DashboardPage></DashboardPage>}
          ></Route>
          <Route
            exact
            path="*"
            element={<DashboardPage></DashboardPage>}
          ></Route>
          {/* Customer Table */}
          <Route
            exact
            path="/customer"
            element={<CustomerTable></CustomerTable>}
          ></Route>
          {/* Product Table */}
          <Route
            exact
            path="/product"
            element={<ProductTable></ProductTable>}
          ></Route>
          {/* Order Table */}
          <Route
            exact
            path="/order"
            element={<OrderTable></OrderTable>}
          ></Route>
        </Routes>
      </Box>
    </Box>
  );
}

export default MainContent;
