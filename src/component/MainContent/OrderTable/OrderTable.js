import React, { useEffect, useState } from "react";
import {
  Container,
  Grid,
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Box,
  Button,
  Stack,
  Pagination,
  TextField,
  Typography,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Autocomplete,
} from "@mui/material";

// Icon
import { Add, Search, Visibility } from "@mui/icons-material";

// Component
import BreadcrumbComponent from "../../BreadcrumbComponent/BreadcrumbComponent";
import OrderViewModal from "../../Modal/OrderModal/OrderViewModal";
import OrderDetailModal from "../../Modal/OrderModal/OrderDetailModal";
import DeleteConfirmModal from "../../Modal/DeleteConfirmModal";
import AddOrderModal from "../../Modal/OrderModal/AddOrderModal";
import ModalError from "../../Modal/ModalError";
import SnackBar from "../../SnackBar/SnackBar";
function OrderTable() {
  // Fetch Api
  const fetchApi = async (paramUrl, paramOption = {}) => {
    const response = await fetch(paramUrl, paramOption);
    const responseData = await response.json();
    return responseData;
  };
  // State
  const [orders, setOrders] = useState("");
  const [order, setOrder] = useState("");
  const [products, setProducts] = useState("");
  const [product, setProduct] = useState({
    name: "",
  });
  const [productSelected, setProductSelected] = useState([]);
  const [orderDetails, setOrderDetails] = useState("");
  const [orderDetail, setOrderDetail] = useState("");
  const [customers, setCustomers] = useState("");
  const [customer, setCustomer] = useState({
    _id: "",
    address: "",
    city: "",
    country: "",
    email: "",
    fullname: "",
    password: "",
    phoneNumber: "",
    photoURL: "",
    role: "",
    timeCreated: "",
    timeUpdated: "",
  });
  // Modal
  const [modalViewOrder, setModalViewOrder] = useState({
    display: false,
  });
  const [modalOrderDetail, setModalOrderDetail] = useState({
    display: false,
  });
  const [modalDeleteConfirm, setModalDeleteConfirm] = useState({
    display: false,
    content: "",
  });
  const [modalAddOrder, setModalAddOrder] = useState({
    display: false,
  });
  const [modalError, setModalError] = useState({
    display: false,
    content: "",
  });
  // Pagination
  const [limit, setLimit] = useState(4);
  const [numberOfPage, setNumberOfPage] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [countOrder, setCountOrder] = useState(""); //State Đếm tổng số product
  // SnackBar
  const [snackBar, setSnackBar] = useState({
    display: false,
    content: "",
    severity: "",
  });
  // Call API
  // Call API get all product
  const getAllProduct = (paramLimit, paramSkip) => {
    fetchApi("http://localhost:8080/products" + "?limit=" + "" + "&skip=" + "")
      .then((data) => {
        console.log(data);
        setProducts(data.products);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Call Api get order Count
  const getOrderCount = () => {
    fetchApi("http://localhost:8080/orders/count")
      .then((data) => {
        console.log(data.number);
        setCountOrder(data.number); //Call API thành công lưu lại tổng số order
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Call API get all Order
  const getAllOrder = (paramLimit, paramSkip) => {
    fetchApi(
      "http://localhost:8080/orders" +
        "?limit=" +
        paramLimit +
        "&skip=" +
        paramSkip
    )
      .then((data) => {
        console.log(data);
        setOrders(data.orders);
        // Tính số trang
        setNumberOfPage(Math.ceil(countOrder / limit));
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Call API get all customer
  const getAllCustomer = (paramLimit, paramSkip) => {
    fetchApi("http://localhost:8080/customers" + "?limit=" + "" + "&skip=" + "")
      .then((data) => {
        console.log(data);
        setCustomers(data.customers);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Call API get order by customerid
  const getOrderByCustomerId = () => {
    // Call API Search
    fetchApi(
      "http://localhost:8080/orders/filter" +
        "?limit=" +
        "" +
        "&skip=" +
        "" +
        "&customer=" +
        `${customer ? customer._id : ""}`
    )
      .then((data) => {
        console.log(data);
        if (customer === null || customer._id === "") {
          getAllOrder(limit, (currentPage - 1) * limit);
        } else {
          setOrders(data.orders);
          console.log(data.orders);
          setNumberOfPage(1);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Call API get one customer
  const getOneCustomer = (paramCustomerId) => {
    fetchApi("http://localhost:8080/customers/" + paramCustomerId)
      .then((data) => {
        console.log(data);
        setCustomer(data.customer);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Call API get OrderDetails Of Order
  const getAllOrderDetailsOfOrder = (paramOrderId) => {
    fetchApi("http://localhost:8080/orderDetail/" + paramOrderId)
      .then((data) => {
        setOrderDetails(data.orderDetails);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Call API create order
  const callApiCreateOrder = (paramCustomerID, paramOrder, orderDetails) => {
    const orderRequest = {
      customer: paramCustomerID,
      note: paramOrder.note,
      shippedDate: paramOrder.shippedDate,
      orderDetails: orderDetails,
    };
    const order = {
      method: "POST",
      body: JSON.stringify(orderRequest),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    };
    fetchApi("http://localhost:8080/orders", order)
      .then((data) => {
        console.log(data);
        // Đóng modal
        setModalAddOrder({
          display: false,
        });
        // reset state
        setProductSelected([]);
        setCustomer({
          _id: "",
          address: "",
          city: "",
          country: "",
          email: "",
          fullname: "",
          password: "",
          phoneNumber: "",
          photoURL: "",
          role: "",
          timeCreated: "",
          timeUpdated: "",
        });
        setOrder("");
        // reload table
        getAllOrder(limit, (currentPage - 1) * limit);
        setSnackBar({
          display: true,
          content: "Create Order Success",
          severity: "success",
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Call API update order
  const callApiUpdateOrder = (paramOrder) => {
    // Tạo Object request
    const orderObj = {
      method: "PUT",
      body: JSON.stringify(paramOrder),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    };
    fetchApi("http://localhost:8080/orders/" + paramOrder._id, orderObj)
      .then((data) => {
        console.log(data);
        // Reload Bảng
        getAllOrder(limit, (currentPage - 1) * limit);
        // Snack Bar Success
        setSnackBar({
          display: true,
          content: "Update Order Success",
          severity: "success",
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Call API update order detail
  const callApiUpdateOrderDetail = (paramOrderDetail) => {
    // Tạo Object Request
    const orderDetailObj = {
      method: "PUT",
      body: JSON.stringify(paramOrderDetail),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    };
    fetchApi(
      "http://localhost:8080/orderDetail/oneOrderDetail/" +
        paramOrderDetail._id,
      orderDetailObj
    )
      .then((data) => {
        console.log(data);
        // Reload Order Detail
        getAllOrderDetailsOfOrder(data.updatedObj.order);
        // Ẩn modal
        setModalOrderDetail({
          display: false,
        });
        // Hiện Snack Bar Success
        setSnackBar({
          display: true,
          content: "Update Order Detail Success",
          severity: "success",
        });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Call API delete order detail
  const callApiDeleteOrderDetail = (paramOrderDetail) => {
    fetchApi(
      `http://localhost:8080/orderDetail/oneOrderDetail/${paramOrderDetail._id}`,
      { method: "DELETE" }
    )
      .then((data) => {
        console.log(data);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        // Reload Order Details
        getAllOrderDetailsOfOrder(paramOrderDetail.order._id);
        // Ẩn Modal Confim
        setModalDeleteConfirm({
          display: false,
          content: "",
        });
        // Hiển thị Snack Bar Delete
        setSnackBar({
          display: true,
          content: "Delete Order Detail Success",
          severity: "warning",
        });
      });
  };
  // Event Handle
  // Hàm xử lý khi đổi Limit
  const onChangeLimit = (event) => {
    setLimit(event.target.value);
  };
  // Hàm xử lý khi chuyển trang
  const changePageHandle = (event, value) => {
    console.log("Change Page");
    setCurrentPage(value);
  };
  // Hàm Lưu Giá trị khi ô input search order by customer thay đổi
  const onInputCustomer = (event, value) => {
    setCustomer(value);
  };

  // Hàm Lưu Giá trị khi ô input search products thay đổi
  const onInputProduct = (event, value) => {
    setProduct(value);
  };

  // Hàm xử lý sự kiện khi nhấn nút search (Search Order By customer ID)
  const searchOrder = () => {
    // Call API
    getOrderByCustomerId();
  };
  // Hàm xử lý sự kiện khi nhấn nút View
  const onViewButtonClick = (order) => {
    // Hiện Modal
    setModalViewOrder({
      display: true,
    });
    // Set State Order
    setOrder(order);
    // Call API lấy customer Từ order ID
    getOneCustomer(order.customer._id);
    // Call Api lấy tất cả order Detail từ Order ID
    getAllOrderDetailsOfOrder(order._id);
  };
  // Hàm xử lý sự kiện khi nhấn nút Update
  const updateOrder = () => {
    // Call API update order
    callApiUpdateOrder(order);
    // Đóng Modal
    setModalViewOrder({
      display: false,
    });
  };
  // Hàm xử lý sự kiện khi nhấn nút Edit trong Modal View
  const editButtonClick = (orderDetail) => {
    // Lưu Order Detail vào State
    setOrderDetail(orderDetail);
    // Hiện Modal
    setModalOrderDetail({
      display: true,
    });
  };
  // Hàm Xử lý sự kiện khi nhấn nút Confirm trong Order Detail Modal
  const confirmButtonOrderDetailClick = (orderDetail) => {
    // Call API update order detail
    callApiUpdateOrderDetail(orderDetail);
  };
  // Hàm xử lý sự kiện khi nhấn nút Delete
  const deleteButtonClick = (orderDetail) => {
    // Show Modal Delete Order Detail Confirm
    setModalDeleteConfirm({
      display: true,
      content: "Are you sure you want to delete this order detail",
    });
    // Lưu Order Detail vào State
    setOrderDetail(orderDetail);
  };
  // Hàm xử lý sự kiện khi nhấn nút Add
  const addButtonClick = () => {
    setModalAddOrder({
      display: true,
    });
  };

  // Hàm xử lý sự kiên khi nhấn nút Create Order
  const createOrderButtonClick = () => {
    // Validate
    let isValidated = validate();
    if (isValidated) {
      // Call Api create order
      callApiCreateOrder(customer._id, order, productSelected);
    }
  };
  // Hàm validated dữ liệu
  const validate = () => {
    // Validate Customer
    if (customer === null || customer._id === "") {
      setModalError({
        display: true,
        content: "Invalid Customer",
      });
      return false;
    }
    // Validate Product Select
    if (productSelected.length === 0) {
      setModalError({
        display: true,
        content: "Invalid Product",
      });
      return false;
    }
    // Validate Shipped Date
    if (order.shippedDate === undefined || order.shippedDate === null) {
      setModalError({
        display: true,
        content: "Invalid Shipped Date",
      });
      return false;
    }
    return true;
  };
  // useEffect được gọi lại khi các state trong dependencies thay đổi
  useEffect(() => {
    getOrderCount();
    getAllCustomer();
    getAllProduct();
    getAllOrder(limit, (currentPage - 1) * limit);
  }, [countOrder, limit, currentPage]);
  return (
    <>
      <Container maxWidth="large">
        {/* Breadcrumb , Truyền content vào Breadcrumb*/}
        <BreadcrumbComponent content="Order"></BreadcrumbComponent>
        {/* Title */}
        <Typography
          fontWeight="bold"
          my={2}
          align="center"
          variant="h3"
          component="h2"
        >
          Order Manager
        </Typography>
        {/* Search Form */}
        <Grid className="search-form-customer" mb={4} container>
          {/* Full Name */}
          <Grid p={1} item xs={12}>
            <Autocomplete
              disablePortal
              options={customers}
              value={customer}
              onChange={onInputCustomer}
              getOptionLabel={(option) => option.fullname}
              renderInput={(params) => (
                <TextField {...params} label="Search Order By Customer" />
              )}
              isOptionEqualToValue={(option, value) => option === value}
              className="w-100 background-input"
            />
          </Grid>
          {/* Search Button */}
          <Grid p={1} item xs={12}>
            <Button
              sx={{ float: "right", fontWeight: "bold" }}
              size="large"
              variant="contained"
              onClick={() => {
                searchOrder();
              }}
              startIcon={<Search />}
            >
              Search
            </Button>
          </Grid>
        </Grid>

        <Grid my={2} alignItems="end" container>
          {/* Pagination */}
          <Grid item xs={6}>
            <Stack spacing={2}>
              <Pagination
                onChange={changePageHandle}
                count={numberOfPage}
                defaultPage={currentPage}
                color="primary"
              />
            </Stack>
          </Grid>
          <Grid item xs={6}>
            <Grid container>
              <Grid item className="d-flex justify-content-end" xs={12}>
                {/* Select Limit */}
                <FormControl>
                  <InputLabel id="select-limit">Limit</InputLabel>
                  <Select
                    labelId="select-limit"
                    value={limit}
                    label="Limit"
                    onChange={onChangeLimit}
                    className="background-input"
                  >
                    <MenuItem value="4">4</MenuItem>
                    <MenuItem value="6">6</MenuItem>
                    <MenuItem value="10">10</MenuItem>
                  </Select>
                </FormControl>
                {/* Add Product Button */}
                <Button
                  sx={{ ml: 1, fontWeight: "bold", float: "right" }}
                  variant="outlined"
                  size="large"
                  startIcon={<Add />}
                  onClick={addButtonClick}
                >
                  Add Order
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        {/* Table */}
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} size="large" aria-label="a dense table">
            <TableHead>
              <TableRow>
                <TableCell>#</TableCell>
                <TableCell className="fw-bold" align="left">
                  Customer
                </TableCell>
                <TableCell className="fw-bold" align="left">
                  Order Date
                </TableCell>
                <TableCell className="fw-bold" align="left">
                  Shipped Date
                </TableCell>
                <TableCell className="fw-bold" align="left">
                  Note
                </TableCell>
                <TableCell className="fw-bold" align="center">
                  Status
                </TableCell>
                <TableCell className="fw-bold" align="center">
                  Action
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {orders !== ""
                ? orders.map((order, index) => (
                    <TableRow
                      key={index}
                      sx={{
                        "&:last-child td, &:last-child th": { border: 0 },
                        maxHeight: "100px",
                        overflow: "hidden",
                      }}
                    >
                      <TableCell component="th" scope="row">
                        {index + 1}
                      </TableCell>
                      <TableCell align="left">
                        {order.customer.fullname}
                      </TableCell>
                      {/* Format Date Type */}
                      <TableCell align="left">
                        {new Date(order.orderDate).toLocaleDateString("vi-VN")}{" "}
                      </TableCell>
                      {/* Format Date Type */}
                      <TableCell align="left">
                        {order.shippedDate === undefined
                          ? ""
                          : new Date(order.shippedDate).toLocaleDateString(
                              "vi-VN"
                            )}
                      </TableCell>
                      <TableCell
                        sx={{
                          width: "200px",
                          whiteSpace: "nowrap",
                        }}
                        align="left"
                      >
                        <Box
                          component="div"
                          sx={{
                            width: "inherit",
                            textOverflow: "ellipsis",
                            overflow: "hidden",
                          }}
                        >
                          {order.note}
                        </Box>
                      </TableCell>
                      <TableCell align="center">
                        {order.status === 0 ? "Open" : null}
                        {order.status === 1 ? "Confirm" : null}
                        {order.status === 2 ? "Cancel" : null}
                      </TableCell>
                      <TableCell align="center">
                        <Button
                          sx={{ fontWeight: "bold" }}
                          variant="contained"
                          size="small"
                          startIcon={<Visibility />}
                          onClick={() => {
                            onViewButtonClick(order);
                          }}
                        >
                          VIEW
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))
                : null}
            </TableBody>
          </Table>
        </TableContainer>
      </Container>
      {/* Modal */}
      {/* View */}
      <OrderViewModal
        modalViewOrder={modalViewOrder}
        setModalViewOrder={setModalViewOrder}
        order={order}
        setOrder={setOrder}
        orderDetails={orderDetails}
        customer={customer}
        setCustomer={setCustomer}
        updateOrder={updateOrder}
        editButtonClick={editButtonClick}
        deleteButtonClick={deleteButtonClick}
      ></OrderViewModal>
      {/* Detail */}
      <OrderDetailModal
        orderDetail={orderDetail}
        setOrderDetail={setOrderDetail}
        modalOrderDetail={modalOrderDetail}
        setModalOrderDetail={setModalOrderDetail}
        confirmButtonOrderDetailClick={confirmButtonOrderDetailClick}
      ></OrderDetailModal>
      {/* Delete Confirm */}
      <DeleteConfirmModal
        modalDeleteConfirm={modalDeleteConfirm}
        setModalDeleteConfirm={setModalDeleteConfirm}
        orderDetail={orderDetail}
        callApiDeleteOrderDetail={callApiDeleteOrderDetail}
      ></DeleteConfirmModal>
      {/* Add */}
      <AddOrderModal
        order={order}
        customers={customers}
        customer={customer}
        productSelected={productSelected}
        modalAddOrder={modalAddOrder}
        products={products}
        product={product}
        onInputProduct={onInputProduct}
        setCustomer={setCustomer}
        setOrder={setOrder}
        setModalAddOrder={setModalAddOrder}
        onInputCustomer={onInputCustomer}
        createOrderButtonClick={createOrderButtonClick}
        setProductSelected={setProductSelected}
        setProduct={setProduct}
      ></AddOrderModal>
      {/* Error */}
      <ModalError
        modalError={modalError}
        setModalError={setModalError}
      ></ModalError>
      {/* Snack Bar */}
      <SnackBar snackBar={snackBar} setSnackBar={setSnackBar}></SnackBar>
    </>
  );
}

export default OrderTable;
