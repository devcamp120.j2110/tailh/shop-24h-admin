// Hook
import React, { useEffect, useState } from "react";
// CSS
import "./ProductTable.css";
// Material UI
import {
  Container,
  Grid,
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Box,
  Button,
  Stack,
  Pagination,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Typography,
} from "@mui/material";
// Style
import { styled } from "@mui/material/styles";
import { red } from "@mui/material/colors";
// Component
import BreadcrumbComponent from "../../BreadcrumbComponent/BreadcrumbComponent";
import EditProductModal from "../../Modal/ProductModal/EditProductModal";
// Icon
import { Add, Delete, Edit, Search } from "@mui/icons-material";
import DeleteConfirmModal from "../../Modal/DeleteConfirmModal";
import AddProductModal from "../../Modal/ProductModal/AddProductModal";
import ModalError from "../../Modal/ModalError";
import SnackBar from "../../SnackBar/SnackBar";
function ProductTable() {
  // Custom Button
  const ButtonDelete = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(red[400]),
    backgroundColor: red[400],
    "&:hover": {
      backgroundColor: red[500],
    },
  }));
  // State
  const [addMode, setAddMode] = useState(false);
  const [products, setProducts] = useState("");
  const [product, setProduct] = useState("");
  const [productNameCurrent, setProductNameCurrent] = useState("");
  const [productNameExists, setProductNameExists] = useState(false);
  const [filterInput, setFilterInput] = useState({
    name: "",
    buyPrice: "",
    color: "",
  });
  // Modal
  const [modalEditProduct, setModalEditProduct] = useState({
    display: false,
  });
  const [modalAddProduct, setModalAddProduct] = useState({
    display: false,
  });
  const [modalDeleteConfirm, setModalDeleteConfirm] = useState({
    display: false,
    content: "",
  });
  const [modalError, setModalError] = useState({
    display: false,
    content: "",
  });
  // Pagination
  const [limit, setLimit] = useState(4);
  const [numberOfPage, setNumberOfPage] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [countProduct, setCountProduct] = useState(""); //State Đếm tổng số product
  // SnackBar
  const [snackBar, setSnackBar] = useState({
    display: false,
    content: "",
    severity: "",
  });
  // Fetch Api
  const fetchApi = async (paramUrl, paramOption = {}) => {
    const response = await fetch(paramUrl, paramOption);
    const responseData = await response.json();
    return responseData;
  };
  // Call API
  // Get Product Count
  const getProductCount = () => {
    fetchApi("http://localhost:8080/products/count")
      .then((data) => {
        console.log(data.number);
        setCountProduct(data.number); //Call API thành công lưu lại tổng số product
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Call API get all product
  const getAllProduct = (paramLimit, paramSkip) => {
    fetchApi(
      "http://localhost:8080/products" +
        "?limit=" +
        paramLimit +
        "&skip=" +
        paramSkip
    )
      .then((data) => {
        console.log(data);
        // Lưu giá trị vào State
        setProducts(data.products);
        // Tính số trang
        setNumberOfPage(Math.ceil(countProduct / limit));
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Call API search sản phẩm
  const callApiSearchProduct = () => {
    fetchApi(
      "http://localhost:8080/products/filter" +
        "?limit=" +
        "" +
        "&skip=" +
        "" +
        "&name=" +
        filterInput.name.toLowerCase() +
        "&buyPrice=" +
        filterInput.buyPrice +
        "&color=" +
        filterInput.color
    )
      .then((data) => {
        console.log(data);
        if (
          filterInput.name === "" &&
          filterInput.buyPrice === "" &&
          filterInput.color === ""
        ) {
          // TH input rỗng thì reload bảng
          getAllProduct(limit, (currentPage - 1) * limit);
        } else {
          // TH input có giá trị thì reload bảng vs respon
          setProducts(data.products);
          setNumberOfPage(1);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Call API kiểm tra tên sản phẩm
  const callApiCheckProductNameExists = () => {
    fetchApi("http://localhost:8080/products/name/" + product.name)
      .then((data) => {
        console.log(data);
        if (data.product !== null) {
          // Trường hợp tìm thấy sản phẩm
          setProductNameExists(true);
        } else {
          // Trường hợp Không tìm thấy sản phẩm
          setProductNameExists(false);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Call API create sản phẩm
  const callApiCreateProduct = (paramObject) => {
    // Tạo Object Request
    const productObj = {
      method: "POST",
      body: JSON.stringify(paramObject),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    };
    fetchApi("http://localhost:8080/products/", productObj)
      .then((data) => {
        console.log(data);
        // Ẩn modal Add
        setModalAddProduct({
          display: false,
        });
        // Reload
        getAllProduct(limit, (currentPage - 1) * limit);
        setCurrentPage(1);
        // SnackBar
        setSnackBar({
          display: true,
          content: "Create Product Success",
          severity: "success",
        });
        // Rs state
        setProduct("");
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Call API update sản phẩm
  const callApiUpdateProduct = (paramObject) => {
    // Tạo Object Request
    const productObj = {
      method: "PUT",
      body: JSON.stringify(paramObject),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    };
    fetchApi("http://localhost:8080/products/" + paramObject._id, productObj)
      .then((data) => {
        console.log(data);
        // Đóng modal
        setModalEditProduct({
          display: false,
        });
        // Snack Bar
        setSnackBar({
          display: true,
          content: "Update Product Success",
          severity: "success",
        });
        // Reload
        getAllProduct(limit, (currentPage - 1) * limit);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  // Call API delete
  const callApiDeleteProduct = (paramObject) => {
    fetchApi(`http://localhost:8080/products/${paramObject._id}`, {
      method: "DELETE",
    })
      .then((data) => {
        console.log(data);
      })
      .catch((err) => {
        console.log(err);
      })
      .finally(() => {
        // Ẩn modal
        setModalDeleteConfirm({
          display: false,
          content: "",
        });
        // Reload
        getAllProduct(limit, 0);
        setCurrentPage(1);
        // Snack Bar
        setSnackBar({
          display: true,
          content: "Delete Product Success",
          severity: "warning",
        });
        // Rs state
        setProduct("");
      });
  };
  // Handle Event
  // Hàm xử lý khi chuyển trang
  const changePageHandle = (event, value) => {
    console.log("Change Page");
    setCurrentPage(value);
    window.scrollTo({ top: 0, behavior: "smooth" });
  };
  // Hàm xử lý khi đổi Limit
  const onChangeLimit = (event) => {
    setLimit(event.target.value);
  };
  // Hàm Lưu Giá trị khi ô input thay đổi
  const onInputName = (event) => {
    setFilterInput({ ...filterInput, name: event.target.value });
  };
  const onInputColor = (event) => {
    setFilterInput({ ...filterInput, color: event.target.value });
  };
  const onInputPrice = (event) => {
    setFilterInput({ ...filterInput, buyPrice: event.target.value });
  };
  // Hàm call API search sản phẩm
  const searchProduct = () => {
    // Call API search
    callApiSearchProduct();
  };
  // Xử lý sự kiện khi nhấn nút Edit
  const onEditButtonClick = (product) => {
    // Hiển thị modal và truyền product
    setModalEditProduct({
      display: true,
    });
    // Lưu lại product vào state
    setProduct(product);
    // Lưu lại product name hiện tại
    setProductNameCurrent(product.name);
    // set add mode
    setAddMode(false);
  };
  // Xử lý sự kiện khi nhấn nút Delete
  const onDeleteButtonClick = (product) => {
    // Hiện modal confirm
    setModalDeleteConfirm({
      display: true,
      content: "Are you sure you want to delete product",
    });
    // Lưu lại product vào state
    setProduct(product);
  };
  // Xử lý sự kiện khi nhấn nút Add
  const onAddButtonClick = () => {
    // Hiện modal
    setModalAddProduct({
      display: true,
    });
    // set add mode
    setAddMode(true);
  };
  // Xử ly sự kiện khi nhấn nút Update
  const onUpdateButtonClick = () => {
    // validate dữ liệu
    let isValidated = validateData();
    if (isValidated) {
      // Call API Update Product
      callApiUpdateProduct(product);
    }
  };
  // Xử lý sự kiện khi nhấn nút Confirm Delete
  const onConfirmDeleteButtonClick = () => {
    // Call Api Delete Product
    callApiDeleteProduct(product);
  };
  // xử lý sự kiên khi nhấn nút Create Product
  const onCreateProductButtonClick = () => {
    // call API
    var isValidated = validateData();
    if (isValidated) {
      // call api create product
      callApiCreateProduct(product);
    }
  };

  // Hàm validate dữ liệu
  const validateData = () => {
    // Validate Trường hợp thêm customer
    if (addMode) {
      if (product.name === "" || productNameExists === true) {
        setModalError({
          display: true,
          content: "Invalid Product Name",
        });
        return false;
      }
    }
    // Validate Trường hợp edit customer
    if (!addMode) {
      if (
        product.name === "" ||
        (productNameExists === true &&
          product.name.trim() !== productNameCurrent.trim())
      ) {
        setModalError({
          display: true,
          content: "Invalid Product Name",
        });
        return false;
      }
    }
    // Validate Color
    if (product.color === "") {
      setModalError({
        display: true,
        content: "Invalid Car Color",
      });
      return false;
    }
    // Validate Type
    if (product.type === "") {
      setModalError({
        display: true,
        content: "Invalid Car Type",
      });
      return false;
    }
    // Validate Buy Price
    if (product.buyPrice === "" || isNaN(product.buyPrice)) {
      setModalError({
        display: true,
        content: "Invalid Buy Price",
      });
      return false;
    }
    // Validate Promotion Price
    if (product.promotionPrice === "" || isNaN(product.promotionPrice)) {
      setModalError({
        display: true,
        content: "Invalid Promotion Price",
      });
      return false;
    }
    // Validate Image URL
    if (product.imageUrl === "") {
      setModalError({
        display: true,
        content: "Invalid Image URL",
      });
      return false;
    }
    // Validate Description
    if (product.description === "") {
      setModalError({
        display: true,
        content: "Invalid Description",
      });
      return false;
    }
    // Validate Thumnail
    var checkThumbnail = (item) => {
      return item !== "";
    };
    if (!product.thumbnailUrl.every(checkThumbnail)) {
      setModalError({
        display: true,
        content: "Invalid Thumbnail URL",
      });
      return false;
    }
    return true;
  };
  // useEffect gọi lại khi currentPage, countProduct,limit thay đổi
  useEffect(() => {
    getProductCount();
    getAllProduct(limit, (currentPage - 1) * limit);
    callApiCheckProductNameExists();
  }, [currentPage, countProduct, limit, product]);

  return (
    <>
      <Container maxWidth="large">
        {/* Breadcrumb , Truyền content vào Breadcrumb*/}
        <BreadcrumbComponent content="Product"></BreadcrumbComponent>
        {/* Title */}
        <Typography
          fontWeight="bold"
          my={2}
          align="center"
          variant="h3"
          component="h2"
        >
          Product Manager
        </Typography>
        {/* Search Form */}
        <Grid className="search-form-product" mb={4} container>
          {/* Name */}
          <Grid p={1} item xs={6}>
            <TextField
              id="outlined-name"
              className="w-100 background-input"
              label="Name"
              value={filterInput.name}
              onChange={onInputName}
            />
          </Grid>
          {/* Color */}
          <Grid p={1} item xs={6}>
            <FormControl fullWidth>
              <InputLabel id="select-color">Color</InputLabel>
              <Select
                labelId="select-color"
                value={filterInput.color}
                label="Color"
                onChange={onInputColor}
                className="background-input"
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value="red">Red</MenuItem>
                <MenuItem value="black">Black</MenuItem>
                <MenuItem value="blue">Blue</MenuItem>
                <MenuItem value="white">White</MenuItem>
              </Select>
            </FormControl>
          </Grid>
          {/* Buy Price */}
          <Grid p={1} item xs={12}>
            <FormControl fullWidth>
              <InputLabel id="select-buy-price">Buy Price</InputLabel>
              <Select
                labelId="select-buy-price"
                value={filterInput.buyPrice}
                label="Buy Price"
                onChange={onInputPrice}
                className="background-input"
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value="high">{`High Price (>=$5000)`}</MenuItem>
                <MenuItem value="low">{`Low Price (<=$5000)`}</MenuItem>
              </Select>
            </FormControl>
          </Grid>
          {/* Search Button */}
          <Grid p={1} item xs={12}>
            <Button
              sx={{ float: "right", fontWeight: "bold" }}
              size="large"
              variant="contained"
              onClick={() => {
                searchProduct();
              }}
              startIcon={<Search />}
            >
              Search
            </Button>
          </Grid>
        </Grid>
        <Grid my={2} alignItems="end" container>
          {/* Pagination */}
          <Grid item xs={6}>
            <Stack spacing={2}>
              <Pagination
                onChange={changePageHandle}
                count={numberOfPage}
                defaultPage={currentPage}
                color="primary"
              />
            </Stack>
          </Grid>
          <Grid item xs={6}>
            <Grid container>
              <Grid className="d-flex justify-content-end" item xs={12}>
                {/* Select Limit */}
                <FormControl>
                  <InputLabel id="select-limit">Limit</InputLabel>
                  <Select
                    labelId="select-limit"
                    value={limit}
                    label="Limit"
                    onChange={onChangeLimit}
                    className="background-input"
                  >
                    <MenuItem value="4">4</MenuItem>
                    <MenuItem value="6">6</MenuItem>
                    <MenuItem value="10">10</MenuItem>
                  </Select>
                </FormControl>
                {/* Add Product Button */}
                <Button
                  sx={{ ml: 1, fontWeight: "bold", float: "right" }}
                  variant="outlined"
                  size="large"
                  startIcon={<Add />}
                  onClick={onAddButtonClick}
                >
                  Add Product
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        {/* Table */}
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} size="large" aria-label="a dense table">
            <TableHead>
              <TableRow>
                <TableCell>#</TableCell>
                <TableCell className="fw-bold" align="left">
                  Name
                </TableCell>
                <TableCell className="fw-bold" align="left">
                  Color
                </TableCell>
                <TableCell className="fw-bold" align="center">
                  Type
                </TableCell>
                <TableCell className="fw-bold" align="center">
                  Buy Price ($)
                </TableCell>
                <TableCell className="fw-bold" align="center">
                  Promotion Price ($)
                </TableCell>
                <TableCell className="fw-bold" align="left">
                  Description
                </TableCell>
                <TableCell className="fw-bold" align="center">
                  Action
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {products !== ""
                ? products.map((product, index) => (
                    <TableRow
                      key={product.name}
                      sx={{
                        "&:last-child td, &:last-child th": { border: 0 },
                        maxHeight: "100px",
                        overflow: "hidden",
                      }}
                    >
                      <TableCell component="th" scope="row">
                        {index + 1}
                      </TableCell>
                      <TableCell className="text-capitalize" align="left">
                        {product.name}
                      </TableCell>
                      <TableCell className="text-uppercase" align="left">
                        {product.color}
                      </TableCell>
                      <TableCell className="text-uppercase" align="center">
                        {product.type}
                      </TableCell>
                      <TableCell align="center">{product.buyPrice}</TableCell>
                      <TableCell align="center">
                        {product.promotionPrice}
                      </TableCell>
                      <TableCell
                        sx={{
                          maxWidth: "140px",
                        }}
                        align="left"
                      >
                        <Box
                          component="div"
                          className="custom-text"
                          sx={{
                            textOverflow: "ellipsis",
                            overflow: "hidden",
                            maxHeight: "20px",
                            maxWidth: "100%",
                          }}
                        >
                          {product.description}
                        </Box>
                      </TableCell>
                      <TableCell align="center">
                        {/* Edit Button */}
                        <Button
                          sx={{ fontWeight: "bold" }}
                          variant="contained"
                          size="small"
                          onClick={() => {
                            onEditButtonClick(product);
                          }}
                          startIcon={<Edit />}
                        >
                          EDIT
                        </Button>
                        {/* Delete Button */}
                        <ButtonDelete
                          sx={{ ml: 1, fontWeight: "bold" }}
                          variant="contained"
                          size="small"
                          startIcon={<Delete />}
                          onClick={() => {
                            onDeleteButtonClick(product);
                          }}
                        >
                          DELETE
                        </ButtonDelete>
                      </TableCell>
                    </TableRow>
                  ))
                : null}
            </TableBody>
          </Table>
        </TableContainer>
      </Container>

      {/* Modal Product */}
      {/* Edit */}
      <EditProductModal
        modalEditProduct={modalEditProduct}
        setModalEditProduct={setModalEditProduct}
        product={product}
        setProduct={setProduct}
        onUpdateButtonClick={onUpdateButtonClick}
        callApiUpdateProduct={callApiUpdateProduct}
      ></EditProductModal>
      {/* Delete */}
      <DeleteConfirmModal
        setProduct={setProduct}
        modalDeleteConfirm={modalDeleteConfirm}
        setModalDeleteConfirm={setModalDeleteConfirm}
        onConfirmDeleteButtonClick={onConfirmDeleteButtonClick}
      ></DeleteConfirmModal>
      {/* Add */}
      <AddProductModal
        product={product}
        setProduct={setProduct}
        modalAddProduct={modalAddProduct}
        setModalAddProduct={setModalAddProduct}
        onCreateProductButtonClick={onCreateProductButtonClick}
      ></AddProductModal>
      {/* Modal Error */}
      <ModalError
        modalError={modalError}
        setModalError={setModalError}
      ></ModalError>
      {/* Snack Bar */}
      <SnackBar snackBar={snackBar} setSnackBar={setSnackBar}></SnackBar>
    </>
  );
}
export default ProductTable;
