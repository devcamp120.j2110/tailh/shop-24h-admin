import {
  Button,
  TextField,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
} from "@mui/material";

function CustomerFormModal(props) {
  // Onchange input
  const onChangeEmail = (event) => {
    props.setCustomer({ ...props.customer, email: event.target.value });
  };
  const onChangeFullname = (event) => {
    props.setCustomer({ ...props.customer, fullname: event.target.value });
  };
  const onChangePhoneNumber = (event) => {
    props.setCustomer({ ...props.customer, phoneNumber: event.target.value });
  };
  const onChangeAddress = (event) => {
    props.setCustomer({ ...props.customer, address: event.target.value });
  };
  const onChangeCity = (event) => {
    props.setCustomer({ ...props.customer, city: event.target.value });
  };
  const onChangeCountry = (event) => {
    props.setCustomer({ ...props.customer, country: event.target.value });
  };

  return (
    <Dialog
      open={props.modalCustomer.display}
      onClose={() => {
        props.setModalCustomer({
          display: false,
        });
      }}
      fullWidth="xl"
    >
      <DialogTitle>Customer Info</DialogTitle>
      <DialogContent>
        {/* Email */}
        <TextField
          autoFocus
          margin="dense"
          id="name"
          label="Email"
          type="email"
          fullWidth
          variant="outlined"
          sx={{mb:1}}
          onChange={onChangeEmail}
          value={props.customer?.email}
        />
        {/* Name */}
        <TextField
          margin="dense"
          id="name"
          label="Fullname"
          type="email"
          disabled={false}
          fullWidth
          variant="outlined"
          sx={{mb:1}}
          onChange={onChangeFullname}
          value={props.customer?.fullname}
        />
        {/* Phone Number */}
        <TextField
          margin="dense"
          id="name"
          label="Phone Number"
          type="email"
          disabled={false}
          fullWidth
          variant="outlined"
          sx={{mb:1}}
          onChange={onChangePhoneNumber}
          value={props.customer?.phoneNumber}
        />
        {/* Address */}
        <TextField
          margin="dense"
          id="name"
          label="Address"
          type="email"
          disabled={false}
          fullWidth
          variant="outlined"
          sx={{mb:1}}
          onChange={onChangeAddress}
          value={props.customer?.address}
        />
        {/* City */}
        <TextField
          margin="dense"
          id="name"
          label="City"
          type="email"
          disabled={false}
          fullWidth
          variant="outlined"
          sx={{mb:1}}
          onChange={onChangeCity}
          value={props.customer?.city}
        />
        {/* Country */}
        <TextField
          margin="dense"
          id="name"
          label="Country"
          type="email"
          disabled={false}
          fullWidth
          variant="outlined"
          sx={{mb:1}}
          onChange={onChangeCountry}
          value={props.customer?.country}
        />
      </DialogContent>
      <DialogActions>
        <Button
          variant="outlined"
          onClick={() => {
            props.setModalCustomer({
              display: false,
            });
          }}
        >
          Cancel
        </Button>
        {props.addMode === true ? (
          // TH Create Customer
          <Button
            sx={{ fontWeight: "bold" }}
            variant="contained"
            color="success"
            onClick={props.onCreateCustomerClick}
          >
            Create
          </Button>
        ) : (
          // TH Update Customer
          <Button
            sx={{ fontWeight: "bold" }}
            variant="contained"
            onClick={props.onUpdateCustomerClick}
          >
            Update Customer
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
}

export default CustomerFormModal;
