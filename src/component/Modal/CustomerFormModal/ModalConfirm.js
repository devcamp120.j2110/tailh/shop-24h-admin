import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";

function ModalConfirm(props) {
  // Hàm Đóng Modal
  const handleClose = () => {
    props.setModalConfirm({
      display: false,
      content: "",
    });
  };
  // Hàm xử lý sự kiện khi nhấn nút Confirm Update
  const onConfirmUpdateClick = () => {
    // Call Api Update User
    props.callApiUpdateCustomer(props.customer);
    // Đóng Modal
    handleClose();
    // Hiện SnackBar
    props.setSnackBar({
      display: true,
      content: "Update Customer Success!",
      severity: "success",
    });
  };

  // Hàm xử lý sự kiện khi nhấn nút Confirm Create
  const onConfirmCreateClick = () => {
    // Call Api Create User
    props.callApiCreateCustomer(props.customer);
    // Đóng Modal
    handleClose();
    // Hiện SnackBar
    props.setSnackBar({
      display: true,
      content: "Create Customer Success!",
      severity: "success",
    });
  };
  return (
    <Dialog
      open={props.modalConfirm.display}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{"Confirm"}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {props.modalConfirm.content}?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Disagree</Button>
       
        {props.addMode ? (
          //TH Create Customer
          <Button
            variant="contained"
            color="success"
            onClick={onConfirmCreateClick}
            autoFocus
          >
            Confirm Create
          </Button>
        ) : (
          //TH Update Customer
          <Button variant="contained" onClick={onConfirmUpdateClick} autoFocus>
            Confirm Update
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
}

export default ModalConfirm;
