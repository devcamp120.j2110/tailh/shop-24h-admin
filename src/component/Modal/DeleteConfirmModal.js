// Material UI
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
} from "@mui/material";
function DeleteConfirmModal(props) {
  // Hàm xử lý sự kiện khi đóng modal
  const handleClose = () => {
    // Đóng modal
    props.setModalDeleteConfirm({
      display: false,
      content: "",
    });
    // reset State
    props.setProduct("");
  };

  return (
    <Dialog
      open={props.modalDeleteConfirm.display}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{"Confirm"}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {props.modalDeleteConfirm.content}?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Disagree</Button>
        {props.orderDetail ? (
          // TH delete order detail
          <Button
            onClick={() => {
              props.callApiDeleteOrderDetail(props.orderDetail);
            }}
            color="error"
            variant="contained"
          >
            Confirm Delete
          </Button>
        ) : (
          // TH delete Product
          <Button
            onClick={props.onConfirmDeleteButtonClick}
            color="error"
            variant="contained"
          >
            Confirm Delete
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
}

export default DeleteConfirmModal;
