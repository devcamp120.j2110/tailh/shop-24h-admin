// Material UI
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from "@mui/material";

function ModalError(props) {
  // hàm xử lý khi ẩn modal
  const handleClose = () => {
    props.setModalError({
      ...props.modalError,
      display: false,
    });
  };
  return (
    <Dialog
      open={props.modalError.display}
      onClose={handleClose}
      fullWidth={true}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle color="warning" sx={{color:"danger"}} id="alert-dialog-title">
        {"Warning !!!!"}
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {props.modalError.content} , please try agian.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button  color="error" variant="contained" onClick={handleClose} autoFocus>
          Agree
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default ModalError;
