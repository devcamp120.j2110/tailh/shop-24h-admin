import {
  Grid,
  Button,
  TextField,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  FormControl,
  Typography,
  Autocomplete,
  Divider,
} from "@mui/material";

import DatePicker from "@mui/lab/DatePicker";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";

function AddOrderModal(props) {
  // Hàm xử lý khi ẩn modal
  const handleClose = () => {
    // reset State
    props.setModalAddOrder({
      display: false,
    });
    props.setOrder({
      shippedDate: undefined,
      note: "",
    });
    props.setCustomer({
      _id: "",
      address: "",
      city: "",
      country: "",
      email: "",
      fullname: "",
      password: "",
      phoneNumber: "",
      photoURL: "",
      role: "",
      timeCreated: "",
      timeUpdated: "",
    });
    props.setProductSelected([]);
  };
  // Thêm Order Detail
  const handleAddCart = () => {
    // Hàm xử lý sự kiện khi thêm vào OrderDetail
    let check = props.productSelected.some((product) => {
      if (product.product._id === props.product._id) {
        return true;
      }
    });
    if (!check && props.product.name !== "") {
      props.setProductSelected([
        ...props.productSelected,
        {
          product: props.product,
          quantity: 1,
          priceEach: props.product.promotionPrice,
        },
      ]);
    }
  };
  // Xoá Order Detail
  const handleRemoveProductSelected = (product) => {
    let filterArr = props.productSelected.filter((productSelected) => {
      return productSelected !== product;
    });
    // lưu giá trị vào state
    props.setProductSelected(filterArr);
  };
  return (
    <Dialog
      open={props.modalAddOrder.display}
      fullWidth={true}
      onClose={handleClose}
    >
      <DialogTitle>
        <Typography fontWeight="bold" variant="h4" component="div">
          Add New Order
        </Typography>
      </DialogTitle>
      <Divider></Divider>
      <DialogContent>
        <Grid container>
          {/* Select Customer */}
          <Typography mb={1} fontWeight="bold" variant="h6" component="div">
            Select Customer
          </Typography>
          <Grid item xs={12} mb={1}>
            <Autocomplete
              disablePortal
              options={props.customers}
              value={props.customer}
              onChange={props.onInputCustomer}
              getOptionLabel={(option) => option.fullname}
              renderInput={(params) => (
                <TextField {...params} label="Search Customer" />
              )}
              isOptionEqualToValue={(option, value) => option === value}
            />
          </Grid>
          {/* Cart Auto Complete */}
          <Typography fontWeight="bold" variant="h6" component="div">
            Cart
          </Typography>
          <Grid my={1} alignItems="center" container>
            <Grid item xs={8}>
              <Autocomplete
                disablePortal
                options={props.products}
                value={props.product}
                onChange={props.onInputProduct}
                getOptionLabel={(option) => option.name}
                renderInput={(params) => (
                  <TextField {...params} label="Search Product" />
                )}
                isOptionEqualToValue={(option, value) => option === value}
                className="w-100 background-input"
              />
            </Grid>
            <Grid align="center" item xs={4}>
              {/* Button */}
              <Button
                onClick={handleAddCart}
                variant="contained"
                size="large"
                color="success"
              >
                Add Product
              </Button>
            </Grid>
          </Grid>
          {/* Product Selected */}
          <Grid my={1} item xs={12}>
            {props.productSelected.length !== 0
              ? props.productSelected.map((product, index) => {
                  return (
                    <Grid key={index} container>
                      <Grid
                        className="d-flex justify-content-between"
                        item
                        xs={4}
                      >
                        <Typography
                          sx={{ textTransform: "capitalize" }}
                          variant="h6"
                          gutterBottom
                          component="div"
                        >
                          {product.product.name}
                        </Typography>
                        <Typography variant="h6" gutterBottom component="div">
                          x{product.quantity}
                        </Typography>
                      </Grid>
                      <Grid item xs={4}>
                        <Typography
                          align="center"
                          variant="h6"
                          gutterBottom
                          component="div"
                        >
                          : ${product.priceEach}
                        </Typography>
                      </Grid>
                      <Grid align="right" item xs={4}>
                        <Button
                          sx={{ ml: 1, fontWeight: "bold" }}
                          variant="outlined"
                          size="small"
                          color="error"
                          onClick={() => {
                            handleRemoveProductSelected(product);
                          }}
                        >
                          Delete
                        </Button>
                      </Grid>
                    </Grid>
                  );
                })
              : null}
            {/* Total Price */}
            <Typography
              align="right"
              fontWeight="bold"
              variant="h6"
              component="div"
            >
              {props.productSelected.length !== 0
                ? `Total :$${props.productSelected.reduce((total, current) => {
                    return (total += current.priceEach);
                  }, 0)}`
                : null}
            </Typography>
          </Grid>
          {/* Order Info */}
          <Grid item xs={12} mb={1}>
            <Typography fontWeight="bold" variant="h6" component="div">
              Order Info
            </Typography>
            {/* Note */}
            <TextField
              margin="dense"
              id="name"
              label="Note"
              type="email"
              fullWidth
              variant="outlined"
              value={props.order.note === undefined ? "" : props.order.note}
              onChange={(event) => {
                props.setOrder({
                  ...props.order,
                  note: event.target.value,
                });
              }}
            />
          </Grid>
          {/* Shipped Date */}
          <Grid item xs={12} mb={1}>
            <FormControl fullWidth>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DatePicker
                  inputFormat="dd/MM/yyyy"
                  label="Shipped Date"
                  value={
                    props.order.shippedDate === undefined
                      ? ""
                      : props.order.shippedDate
                  }
                  onChange={(newValue) => {
                    props.setOrder({
                      ...props.order,
                      shippedDate: newValue,
                    });
                  }}
                  renderInput={(params) => <TextField {...params} />}
                />
              </LocalizationProvider>
            </FormControl>
          </Grid>
        </Grid>
      </DialogContent>
      <Divider></Divider>
      {/* Button Group */}
      <DialogActions sx={{ my: 1 }}>
        {/* Cancel Button */}
        <Button onClick={handleClose}>Cancel</Button>
        {/* Create Button */}
        <Button variant="contained" onClick={props.createOrderButtonClick}>
          Create Order
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default AddOrderModal;
