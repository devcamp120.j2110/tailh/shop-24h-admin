import {
  Grid,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Typography,
} from "@mui/material";
import { AddCircle, RemoveCircle } from "@mui/icons-material";

function OrderDetailModal(props) {
  // Hàm xử lý khi đóng modal
  const handleClose = () => {
    props.setModalOrderDetail({
      display: false,
    });
  };
  // Hàm Tăng Số lượng của order Detail
  const incrementQuantity = () => {
    props.setOrderDetail({
      ...props.orderDetail,
      quantity: props.orderDetail.quantity + 1,
      priceEach:
        props.orderDetail.priceEach + props.orderDetail.product.promotionPrice,
    });
  };
  // Hàm Giảm Số lượng của order Detail
  const decrementQuantity = () => {
    // Chỉ Giảm Được Đến 1 Đơn Vị
    if (props.orderDetail.quantity > 1) {
      props.setOrderDetail({
        ...props.orderDetail,
        quantity: props.orderDetail.quantity - 1,
        priceEach:
          props.orderDetail.priceEach -
          props.orderDetail.product.promotionPrice,
      });
    }
  };
  return (
    <Dialog
      open={props.modalOrderDetail.display}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">
        <Typography
          variant="h3"
          align="center"
          fontWeight="bold"
          gutterBottom
          component="div"
          sx={{ textTransform: "capitalize" }}
        >
          Edit Order Detail
        </Typography>
      </DialogTitle>
      <DialogContent>
        <Grid container>
          <Grid item xs={6}>
            <Typography
              variant="h5"
              align="center"
              gutterBottom
              component="div"
              sx={{ textTransform: "capitalize" }}
            >
              Product:
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography
              variant="h5"
              align="center"
              fontWeight="bold"
              gutterBottom
              component="div"
              sx={{ textTransform: "capitalize" }}
            >
              {props.orderDetail !== "" ? props.orderDetail.product.name : null}
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography
              variant="h5"
              align="center"
              gutterBottom
              component="div"
            >
              Quantity:
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography
              variant="h6"
              align="center"
              gutterBottom
              fontWeight="bold"
              component="div"
            >
              <AddCircle
                onClick={incrementQuantity}
                mx={1}
                sx={{ cursor: "pointer" }}
              />{" "}
              {props.orderDetail !== "" ? props.orderDetail.quantity : null}{" "}
              <RemoveCircle
                onClick={decrementQuantity}
                mx={1}
                sx={{ cursor: "pointer" }}
              />
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography
              variant="h5"
              align="center"
              gutterBottom
              component="div"
            >
              Price Each:
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography
              variant="h6"
              align="center"
              gutterBottom
              fontWeight="bold"
              component="div"
              color="error"
            >
              {props.orderDetail !== "" ? `$${props.orderDetail.priceEach}` : null}
            </Typography>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button variant="contained" onClick={() => {props.confirmButtonOrderDetailClick(props.orderDetail)}} autoFocus>
          Confirm
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default OrderDetailModal;
