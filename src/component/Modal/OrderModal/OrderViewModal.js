// Material UI
import {
  Grid,
  Button,
  TextField,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Typography,
  Avatar,
} from "@mui/material";
// Date Picker MUI
import DatePicker from "@mui/lab/DatePicker";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";

function OrderViewModal(props) {
  // Hàm xử lý khi đóng modal
  const handleClose = () => {
    props.setModalViewOrder({
      display: false,
    });
    // reset state
    props.setOrder("");
    props.setCustomer({
      _id: "",
      address: "",
      city: "",
      country: "",
      email: "",
      fullname: "",
      password: "",
      phoneNumber: "",
      photoURL: "",
      role: "",
      timeCreated: "",
      timeUpdated: "",
    });
  };
  return (
    <>
      <Dialog
        open={props.modalViewOrder.display}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        fullWidth={true}
      >
        <DialogTitle id="alert-dialog-title">
          <Typography
            variant="h2"
            align="center"
            fontWeight="bold"
            gutterBottom
            component="div"
          >
            Order Info
          </Typography>
          <Grid alignItems="center" container>
            <Grid item xs={1}>
              <Avatar alt="Cindy Baker" src={props.customer?.photoURL} />
            </Grid>
            <Grid item xs={11}>
              {props.customer?.fullname}
            </Grid>
          </Grid>
        </DialogTitle>
        <DialogContent>
          <Grid container>
            <Grid item xs={12}>
              <Typography
                variant="h5"
                fontWeight="bold"
                gutterBottom
                component="div"
              >
                Order Date:{" "}
                {new Date(props.order?.orderDate).toLocaleDateString("vi-VN")}
              </Typography>
            </Grid>
            {/* Order Details */}
            <Grid item xs={12}>
              <Typography
                variant="h5"
                fontWeight="bold"
                gutterBottom
                component="div"
              >
                Order Details:
              </Typography>
            </Grid>
            {props.orderDetails !== ""
              ? props.orderDetails.map((orderDetail, index) => {
                  return (
                    <Grid key={index} container>
                      <Grid
                        className="d-flex justify-content-between"
                        item
                        xs={4}
                      >
                        <Typography
                          sx={{ textTransform: "capitalize" }}
                          variant="h6"
                          gutterBottom
                          component="div"
                        >
                          {orderDetail.product.name}
                        </Typography>
                        <Typography variant="h6" gutterBottom component="div">
                          x {orderDetail.quantity}
                        </Typography>
                      </Grid>
                      <Grid item xs={4}>
                        <Typography
                          align="center"
                          variant="h6"
                          gutterBottom
                          component="div"
                        >
                          : ${orderDetail.priceEach}
                        </Typography>
                      </Grid>
                      <Grid align="right" item xs={4}>
                        <Button
                          sx={{ fontWeight: "bold" }}
                          variant="outlined"
                          size="small"
                          onClick={() => {
                            props.editButtonClick(orderDetail);
                          }}
                        >
                          Edit
                        </Button>
                        <Button
                          sx={{ ml: 1, fontWeight: "bold" }}
                          variant="outlined"
                          size="small"
                          color="error"
                          onClick={() => {
                            props.deleteButtonClick(orderDetail);
                          }}
                        >
                          Delete
                        </Button>
                      </Grid>
                    </Grid>
                  );
                })
              : null}
            {/* Total Price */}
            <Grid container>
              <Grid item xs={12}>
                <Typography
                  variant="h4"
                  align="right"
                  gutterBottom
                  fontWeight="bold"
                  component="div"
                >
                  {" "}
                  Total:{" "}
                  <span className="text-danger">
                    $
                    {props.orderDetails !== ""
                      ? props.orderDetails.reduce((total, current) => {
                          return (total += current.priceEach);
                        }, 0)
                      : null}
                  </span>
                </Typography>
              </Grid>
            </Grid>
            {/* Status */}
            <FormControl sx={{ mt: 3 }} fullWidth>
              <InputLabel id="demo-simple-select-label">Status</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={props.order?.status}
                label="Status"
                onChange={(event) => {
                  props.setOrder({
                    ...props.order,
                    status: event.target.value,
                  });
                }}
              >
                <MenuItem value={0}>Open</MenuItem>
                <MenuItem value={1}>Confirm</MenuItem>
                <MenuItem value={2}>Cancel</MenuItem>
              </Select>
            </FormControl>
            {/* Note */}
            <FormControl sx={{ mt: 3 }} fullWidth>
              <TextField
                id="outlined-multiline-flexible"
                label="Note"
                multiline
                maxRows={2}
                value={props.order?.note}
                onChange={(event) => {
                  props.setOrder({
                    ...props.order,
                    note: event.target.value,
                  });
                }}
              />
            </FormControl>
            {/* Date Picker */}
            <FormControl sx={{ mt: 3 }} fullWidth>
              <LocalizationProvider dateAdapter={AdapterDateFns}>
                <DatePicker
                  inputFormat="dd/MM/yyyy"
                  disabled={props.order?.status === 2 ? true : false}
                  label="Shipped Date"
                  value={
                    props.order.shippedDate === ""
                      ? new Date()
                      : props.order.shippedDate
                  }
                  onChange={(newValue) => {
                    props.setOrder({
                      ...props.order,
                      shippedDate: newValue,
                    });
                  }}
                  renderInput={(params) => <TextField {...params} />}
                />
              </LocalizationProvider>
            </FormControl>
          </Grid>
        </DialogContent>
        {/* Button Group */}
        <DialogActions>
          {/* Cancel Button */}
          <Button onClick={handleClose}>Cancel</Button>
          {/* Update Button */}
          <Button
            variant="contained"
            onClick={() => {
              props.updateOrder();
            }}
            autoFocus
            fontWeight="bold"
          >
            Update
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
}

export default OrderViewModal;
