import {
  Button,
  TextField,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  TextareaAutosize,
  Typography,
  Divider,
  Grid,
  Stack,
  IconButton,
} from "@mui/material";
import { useState } from "react";
import { Clear } from "@mui/icons-material";

function AddProductModal(props) {
  // State
  const [thumbnailList, setThumbnailList] = useState([]);
  const [thumbnailInput, setThumbnailInput] = useState("");

  // Handle Event
  const onChangeNameProduct = (event) => {
    props.setProduct({ ...props.product, name: event.target.value });
  };
  const onChangeTypeProduct = (event) => {
    props.setProduct({ ...props.product, type: event.target.value });
  };
  const onChangeColorProduct = (event) => {
    props.setProduct({ ...props.product, color: event.target.value });
  };
  const onChangeImageUrlProduct = (event) => {
    props.setProduct({ ...props.product, imageUrl: event.target.value });
  };
  const onChangeBuyPriceProduct = (event) => {
    props.setProduct({ ...props.product, buyPrice: event.target.value });
  };
  const onChangePromotionPriceProduct = (event) => {
    props.setProduct({ ...props.product, promotionPrice: event.target.value });
  };
  const onChangeDescriptionProduct = (event) => {
    props.setProduct({ ...props.product, description: event.target.value });
  };

  const handleClose = () => {
    props.setModalAddProduct({
      display: false,
    });
    if (props.modalAddProduct.display === false) {
      props.setProduct("");
      setThumbnailList([]);
    }
    props.setProduct("");
    setThumbnailList([]);
  };
  const onAddThumbnailButtonClick = () => {
    if (thumbnailInput !== "") {
      thumbnailList.push(thumbnailInput);
      props.setProduct({ ...props.product, thumbnailUrl: thumbnailList });
      setThumbnailInput("");
    }
  };
  const onRemoveThumbnailButtonClick = (index) => {
    let newArr = [...props.product.thumbnailUrl];
    newArr.splice(index, 1);
    setThumbnailList(newArr);
    props.setProduct({ ...props.product, thumbnailUrl: newArr });
  };

  return (
    <Dialog
      open={props.modalAddProduct.display}
      fullWidth={true}
      onClose={handleClose}
    >
      <DialogTitle>
        <Typography p={1} fontWeight="bold" variant="h4" component="div">
          Add New Product
        </Typography>
      </DialogTitle>
      <DialogContent>
        <Grid container>
          {/* Name */}
          <Grid item xs={12}>
            <TextField
              autoFocus
              margin="dense"
              label="Name"
              fullWidth
              variant="outlined"
              sx={{ mb: 1 }}
              value={props.product.name === undefined ? "" : props.product.name}
              onChange={onChangeNameProduct}
            />
          </Grid>
          {/* Type */}
          <Grid item xs={12}>
            <FormControl
              className="w-100"
              variant="outlined"
              sx={{ minWidth: 120, my: 1 }}
            >
              <InputLabel id="dialog-select-color">Type</InputLabel>
              <Select
                margin="normal"
                labelId="dialog-select-color"
                label="Type"
                value={
                  props.product.type === undefined ? "" : props.product.type
                }
                onChange={onChangeTypeProduct}
                className="w-100"
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value="s">S-Class</MenuItem>
                <MenuItem value="a">A-Class</MenuItem>
                <MenuItem value="b">B-Class</MenuItem>
              </Select>
            </FormControl>
          </Grid>
          {/* Color */}
          <Grid item xs={12}>
            <FormControl
              className="w-100"
              variant="outlined"
              sx={{ minWidth: 120, my: 1 }}
            >
              <InputLabel id="dialog-select-color">Color</InputLabel>
              <Select
                margin="normal"
                labelId="dialog-select-color"
                label="Color"
                required
                value={
                  props.product.color === undefined ? "" : props.product.color
                }
                onChange={onChangeColorProduct}
                className="w-100"
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value="red">Red</MenuItem>
                <MenuItem value="black">Black</MenuItem>
                <MenuItem value="white">White</MenuItem>
                <MenuItem value="blue">Blue</MenuItem>
              </Select>
            </FormControl>
          </Grid>
          {/* Image URL */}
          <Grid item xs={12}>
            <TextField
              margin="dense"
              label="Image Url"
              fullWidth
              variant="outlined"
              sx={{ mb: 1 }}
              value={
                props.product.imageUrl === undefined
                  ? ""
                  : props.product.imageUrl
              }
              onChange={onChangeImageUrlProduct}
            />
          </Grid>
          {/* Buy Price */}
          <Grid item xs={12}>
            <TextField
              margin="dense"
              label="Buy Price"
              fullWidth
              variant="outlined"
              sx={{ mb: 1 }}
              value={
                props.product.buyPrice === undefined
                  ? ""
                  : props.product.buyPrice
              }
              onChange={onChangeBuyPriceProduct}
            />
          </Grid>
          {/* Promotion Price */}
          <Grid item xs={12}>
            <TextField
              margin="dense"
              label="Promotion Price"
              fullWidth
              variant="outlined"
              sx={{ mb: 1 }}
              value={
                props.product.promotionPrice === undefined
                  ? ""
                  : props.product.promotionPrice
              }
              onChange={onChangePromotionPriceProduct}
            />
          </Grid>
          {/* Description */}
          <Grid item xs={12}>
            <TextField
              margin="dense"
              label="Description"
              fullWidth
              variant="outlined"
              multiline
              rows={3}
              sx={{ mb: 1 }}
              value={
                props.product.description === undefined
                  ? ""
                  : props.product.description
              }
              onChange={onChangeDescriptionProduct}
            />
          </Grid>
          {/* Thumbnail URL */}
          <Grid item xs={8}>
            <TextField
              margin="dense"
              label="Thumbnail Url"
              fullWidth
              variant="outlined"
              value={thumbnailInput}
              onChange={(event) => {
                setThumbnailInput(event.target.value);
              }}
            />
          </Grid>
          <Grid className="d-flex m-auto justify-content-center" item xs={4}>
            <Button onClick={onAddThumbnailButtonClick} variant="contained">
              Add Thumbnail
            </Button>
          </Grid>
          {props.product.thumbnailUrl !== undefined
            ? props.product.thumbnailUrl.map((thumbnail, index) => {
                return (
                  <Grid key={index} alignItems="center" container>
                    <Grid
                      item
                      xs={8}
                      sx={{
                        width: "100%",
                        whiteSpace: "nowrap",
                      }}
                    >
                      <Typography
                        variant="subtitle2"
                        fontWeight="bold"
                        gutterBottom
                        component="div"
                        sx={{
                          width: "inherit",
                          textOverflow: "ellipsis",
                          overflow: "hidden",
                        }}
                      >
                        {thumbnail}
                      </Typography>
                    </Grid>
                    <Grid item xs={4}>
                      <IconButton
                        aria-label="upload picture"
                        component="span"
                        color="error"
                        onClick={() => {
                          onRemoveThumbnailButtonClick(index);
                        }}
                      >
                        <Clear />
                      </IconButton>
                    </Grid>
                  </Grid>
                );
              })
            : null}
        </Grid>
      </DialogContent>
      <DialogActions sx={{ my: 1 }}>
        <Button onClick={handleClose}>Cancel</Button>
        <Button onClick={props.onCreateProductButtonClick} variant="contained">
          Create Product
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default AddProductModal;
