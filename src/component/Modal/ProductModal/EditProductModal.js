import {
  Button,
  TextField,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  TextareaAutosize,
  Typography,
} from "@mui/material";

function EditProductModal(props) {
  const onChangeName = (e) => {
    props.setProduct({ ...props.product, name: e.target.value.trim().toLowerCase()});
  };
  const onChangeColor = (e) => {
    props.setProduct({ ...props.product, color: e.target.value });
  };
  const onChangeType = (e) => {
    props.setProduct({ ...props.product, type: e.target.value });
  };
  const onChangeBuyPrice = (e) => {
    props.setProduct({ ...props.product, buyPrice: e.target.value });
  };
  const onChangePromotionPrice = (e) => {
    props.setProduct({ ...props.product, promotionPrice: e.target.value });
  };
  const onChangeImageUrl = (e) => {
    props.setProduct({ ...props.product, imageUrl: e.target.value });
  };
  const onChangeDescription = (e) => {
    props.setProduct({ ...props.product, description: e.target.value });
  };
  const onChangeThumbnailUrl = (index, value) => {
    let newArr = [...props.product.thumbnailUrl];
    newArr[index] = value;
    props.setProduct({ ...props.product, thumbnailUrl: newArr });
  };
  return (
    <Dialog
      open={props.modalEditProduct.display}
      onClose={() => {
        props.setModalEditProduct({
          display: false,
        });
        props.setProduct("");
      }}
      fullWidth="xl"
    >
      <DialogTitle>
        <Typography fontWeight="bold" variant="h4" gutterBottom component="div">
          Product Info
        </Typography>
      </DialogTitle>
      <DialogContent>
        {/* Name */}
        <TextField
          autoFocus
          margin="normal"
          id="name"
          label="Name"
          type="email"
          fullWidth
          variant="outlined"
          required
          value={props.product?.name}
          onChange={onChangeName}
        />
        {/* Color */}
        <FormControl
          className="w-100"
          variant="outlined"
          sx={{ minWidth: 120, mt: 2 }}
        >
          <InputLabel id="dialog-select-color">Color</InputLabel>
          <Select
            margin="normal"
            labelId="dialog-select-color"
            label="Color"
            required
            onChange={onChangeColor}
            defaultValue={props.product?.color}
            className="w-100"
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            <MenuItem value="red">Red</MenuItem>
            <MenuItem value="black">Black</MenuItem>
            <MenuItem value="white">White</MenuItem>
            <MenuItem value="blue">Blue</MenuItem>
          </Select>
        </FormControl>
        {/* Type */}
        <FormControl
          className="w-100"
          variant="outlined"
          sx={{ minWidth: 120, mt: 2 }}
        >
          <InputLabel id="dialog-select-color">Type</InputLabel>
          <Select
            margin="normal"
            labelId="dialog-select-color"
            onChange={onChangeType}
            label="Type"
            defaultValue={props.product?.type}
            className="w-100"
          >
            <MenuItem value="">
              <em>None</em>
            </MenuItem>
            <MenuItem value="s">S-Class</MenuItem>
            <MenuItem value="a">A-Class</MenuItem>
            <MenuItem value="b">B-Class</MenuItem>
          </Select>
        </FormControl>
        {/* Buy Price */}
        <TextField
          margin="normal"
          id="name"
          label="Buy Price ($)"
          type="email"
          fullWidth
          variant="outlined"
          required
          value={props.product?.buyPrice}
          onChange={onChangeBuyPrice}
        />
        {/* Promotion Price */}
        <TextField
          margin="normal"
          id="name"
          label="Promotion Price ($)"
          type="email"
          fullWidth
          variant="outlined"
          required
          value={props.product?.promotionPrice}
          onChange={onChangePromotionPrice}
        />
        {/* Image URL */}
        <TextField
          margin="normal"
          id="name"
          label="Image URL"
          type="email"
          fullWidth
          variant="outlined"
          required
          value={props.product?.imageUrl}
          onChange={onChangeImageUrl}
        />
        {/* Thumbnail Image URL */}
        {props.product?.thumbnailUrl?.map((thumbnail, index) => {
          return (
            <TextField
              key={index}
              margin="normal"
              id="name"
              label={`thumbnailUrl ${index + 1}`}
              type="email"
              fullWidth
              variant="outlined"
              required
              value={thumbnail}
              onChange={(event) =>
                onChangeThumbnailUrl(index, event.target.value)
              }
            />
          );
        })}
        {/* Description */}
        <TextField
          margin="normal"
          id="outlined-multiline-static"
          label="Description"
          multiline
          rows={3}
          value={props.product?.description}
          onChange={onChangeDescription}
          variant="outlined"
          className="w-100"
        />
      </DialogContent>
      <DialogActions>
        <Button
          variant="outlined"
          onClick={() => {
            props.setModalEditProduct({
              display: false,
            });
          }}
        >
          Cancel
        </Button>
        <Button variant="contained" onClick={props.onUpdateButtonClick}>
          Update Product
        </Button>
      </DialogActions>
    </Dialog>
  );
}

export default EditProductModal;
