// Material UI
import { Snackbar, Alert } from "@mui/material";
function SnackBar(props) {
  // Hàm xử lý sự kiện khi đóng modal
  const handleClose = () => {
    props.setSnackBar({
      display: false,
      content: "",
      severity: "",
    });
  };
  return (
    <Snackbar
      open={props.snackBar.display}
      autoHideDuration={4000}
      onClose={handleClose}
    >
      <Alert onClose={handleClose} severity={props.snackBar.severity} sx={{ width: "100%" }}>
        {props.snackBar.content}
      </Alert>
    </Snackbar>
  );
}

export default SnackBar;
